package com.nikosstathis.nikosshop.service;

import static com.nikosstathis.nikosshop.util.RandomUtils.*;

import com.nikosstathis.nikosshop.dto.ProductDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.model.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ProductServiceTestCase provides integration tests for all the functionality of the productService
 * All the tests run in the embedded h2 database
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan({"com.nikosstathis.nikosshop.service", "com.nikosstathis.nikosshop.model"})
@TestPropertySource("classpath:application-test.properties")
public class ProductServiceTestCase {

    @Autowired
    private ProductServiceJpa productService;

    @Autowired
    private TestEntityManager entityManager;

    /**
     * inserts 2 products directly using the testEntityManager (not our implementation) and checks that these are retrieved
     */
    @Test
    public void insertAndRetrieveProducts() {
        //given
        final Product aProduct = new Product(null, generateRandomName(), generateRandomPrice());
        final Product anotherProduct = new Product(null, generateRandomName(), generateRandomPrice());

        //when
        entityManager.persist(aProduct);
        entityManager.persist(anotherProduct);
        entityManager.flush();
        final List<ProductDTO> productFound = productService.findAll();

        //then
        Assert.assertEquals(2, productFound.size());
        Assert.assertTrue(productFound.containsAll(
                Stream.of(new ProductDTO(aProduct), new ProductDTO(anotherProduct)).collect(Collectors.toSet()))
        );
    }

    /**
     * persists one new product with random values
     */
    @Test
    public void persistNewProducts() {
        //given
        final String name = generateRandomName();
        final BigDecimal price = generateRandomPrice();

        //when
        final ProductDTO created = productService.create(name, price);

        //then
        Assert.assertEquals(name, created.getName());
        Assert.assertEquals(created.getPrice(), price.setScale(2, RoundingMode.HALF_UP));
        Assert.assertTrue(created.getId() > 0);
    }

    /**
     * we try to store 2 products with the same name but there is a unique constraint so we expect to get an exception
     */
    @Test(expected = DataIntegrityViolationException.class)
    public void throwDataIntegrityViolationExcpetionWhenTryingToPersist2ProductsWithTheSameName() {
        //given
        final String name = generateRandomName();
        final BigDecimal priceForFirst = generateRandomPrice();
        final BigDecimal priceForSecond = generateRandomPrice();

        //when
        final ProductDTO created = productService.create(name, priceForFirst);
        productService.create(name, priceForSecond);
        //then
        //expect the exception to be thrown
    }

    /**
     * stores a product and then updates its values
     */
    @Test
    public void updateAnExistingProduct() throws InvalidEntityIdException {
        //given
        final ProductDTO created = productService.create(generateRandomName(), generateRandomPrice());
        String newName = generateRandomName();
        final BigDecimal newPrice = generateRandomPrice();

        //when
        final ProductDTO updated = productService.update(created.getId(), newName, newPrice);

        //then
        Assert.assertEquals(newName, updated.getName());
        Assert.assertEquals(newPrice.setScale(2, RoundingMode.HALF_UP), updated.getPrice());
    }

    /**
     * we should be able to call the productService.update() method with null value for the name param
     * indicating that we do not want to change it
     */
    @Test
    public void updateProductAllowNullName() throws InvalidEntityIdException {
        //given
        final ProductDTO created = productService.create(generateRandomName(), generateRandomPrice());
        final BigDecimal newPrice = generateRandomPrice();

        //when
        final ProductDTO updated = productService.update(created.getId(), null, newPrice);

        //then
        Assert.assertEquals(created.getName(), updated.getName());
        Assert.assertEquals(newPrice.setScale(2, RoundingMode.HALF_UP), updated.getPrice());
    }

    /**
     * we should be able to call the productService.update() method with null value for the price param
     * indicating that we do not want to change it
     */
    @Test
    public void updateProductAllowNullPrice() throws InvalidEntityIdException {
        //given
        final ProductDTO created = productService.create(generateRandomName(), generateRandomPrice());
        final String newName = generateRandomName();

        //when
        final ProductDTO updated = productService.update(created.getId(), newName, null);

        //then
        Assert.assertEquals(newName, updated.getName());
        Assert.assertEquals(created.getPrice(), updated.getPrice());
    }

}
