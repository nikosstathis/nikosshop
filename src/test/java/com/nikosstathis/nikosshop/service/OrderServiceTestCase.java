package com.nikosstathis.nikosshop.service;

import com.nikosstathis.nikosshop.dto.*;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.util.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OrderServiceTestCase provides integration tests for all the functionality of the orderService.
 * All the tests run in the embedded h2 database
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan({"com.nikosstathis.nikosshop.service", "com.nikosstathis.nikosshop.model"})
@TestPropertySource("classpath:application-test.properties")
public class OrderServiceTestCase {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;

    private ProductDTO existingFirstProduct;
    private ProductDTO existingSecondProduct;

    @Before
    public void init() {
        existingFirstProduct = productService.create(RandomUtils.generateRandomName(), RandomUtils.generateRandomPrice());
        existingSecondProduct = productService.create(RandomUtils.generateRandomName(), RandomUtils.generateRandomPrice());
    }

    @Test
    public void placeOrder() {
        //given
        final String email = RandomUtils.generateRandomEmail();
        final String referenceCode = RandomUtils.generateRandomReferenceCode();
        long quantityForFirstProduct = RandomUtils.generateRandomQuantity();
        long quantityForSecondProduct = RandomUtils.generateRandomQuantity();

        List<ProductQuantityDTO> productQuantityDTOList = new ArrayList<>();
        productQuantityDTOList.add(new ProductQuantityDTO(existingFirstProduct.getId(), quantityForFirstProduct));
        productQuantityDTOList.add(new ProductQuantityDTO(existingSecondProduct.getId(), quantityForSecondProduct));
        OrderCreationDTO orderCreationDTO = new OrderCreationDTO(referenceCode, email, productQuantityDTOList);

        //when
        OrderExistingDTO placedOrder = orderService.placeOrder(orderCreationDTO);

        //then
        Assert.assertEquals(email, placedOrder.getBuyerEmail());
        Assert.assertEquals(referenceCode, placedOrder.getReferenceCode());
        Assert.assertEquals(2, placedOrder.getProducts().size());

        //The total price equals price1*quantity1 +price2*quantity2
        BigDecimal expectedPrice = existingFirstProduct.getPrice().multiply(new BigDecimal(quantityForFirstProduct))
                .add(existingSecondProduct.getPrice().multiply(new BigDecimal(quantityForSecondProduct)));
        Assert.assertEquals(expectedPrice.setScale(2, RoundingMode.HALF_UP), placedOrder.getTotalPrice());
    }

    @Test
    public void recalculateOrderPriceBasedOnCurrentProductPrices() throws InvalidEntityIdException {
        //given
        final String email = RandomUtils.generateRandomEmail();
        final String referenceCode = RandomUtils.generateRandomReferenceCode();
        long quantityForFirstProduct = RandomUtils.generateRandomQuantity();
        long quantityForSecondProduct = RandomUtils.generateRandomQuantity();

        List<ProductQuantityDTO> productQuantityDTOList = new ArrayList<>();
        productQuantityDTOList.add(new ProductQuantityDTO(existingFirstProduct.getId(), quantityForFirstProduct));
        productQuantityDTOList.add(new ProductQuantityDTO(existingSecondProduct.getId(), quantityForSecondProduct));
        OrderCreationDTO orderCreationDTO = new OrderCreationDTO(referenceCode, email, productQuantityDTOList);

        //when
        OrderExistingDTO placedOrder = orderService.placeOrder(orderCreationDTO);
        //now we will update the product prices
        existingFirstProduct = productService.update(existingFirstProduct.getId(), null, RandomUtils.generateRandomPrice());
        existingSecondProduct = productService.update(existingSecondProduct.getId(), null, RandomUtils.generateRandomPrice());
        //we will call the method to recalculate the prices based on the updates
        RecalculateOrderPriceResultDTO result = orderService.recalculateOrderPriceBasedOnCurrentProductPrices(placedOrder.getOrderId());

        //then
        Assert.assertEquals(placedOrder.getTotalPrice(), result.getOldPrice());
        //The total price equals price1*quantity1 +price2*quantity2
        BigDecimal expectedPrice = existingFirstProduct.getPrice().multiply(new BigDecimal(quantityForFirstProduct))
                .add(existingSecondProduct.getPrice().multiply(new BigDecimal(quantityForSecondProduct)));
        Assert.assertEquals(expectedPrice, result.getNewPrice());
    }

    @Test
    public void persistAndFindOrders() {
        //given
        OrderCreationDTO firstOrderCreationDTO = generateOrderCreationDTO();
        OrderCreationDTO secondOrderCreationDTO = generateOrderCreationDTO();

        //when
        orderService.placeOrder(firstOrderCreationDTO);
        orderService.placeOrder(secondOrderCreationDTO);
        List<OrderExistingDTO> existingOrders = orderService.findOrders(new Date(System.currentTimeMillis() - 100000l), new Date(System.currentTimeMillis() + 10000l));

        //then
        Assert.assertEquals(2, existingOrders.size());
        List<String> foundReferenceCodes = existingOrders.stream().map(order -> order.getReferenceCode()).collect(Collectors.toList());
        Assert.assertTrue(foundReferenceCodes.contains(firstOrderCreationDTO.getReferenceCode()));
        Assert.assertTrue(foundReferenceCodes.contains(secondOrderCreationDTO.getReferenceCode()));
    }

    @Test
    public void persistAndFindOrdersWithNullDates() {
        //given
        OrderCreationDTO firstOrderCreationDTO = generateOrderCreationDTO();
        OrderCreationDTO secondOrderCreationDTO = generateOrderCreationDTO();

        //when
        orderService.placeOrder(firstOrderCreationDTO);
        orderService.placeOrder(secondOrderCreationDTO);
        List<OrderExistingDTO> existingOrders = orderService.findOrders(null, null);

        //then
        Assert.assertEquals(2, existingOrders.size());
    }

    @Test
    public void persistAndFindOrdersWithNullDateTo() {
        //given
        OrderCreationDTO firstOrderCreationDTO = generateOrderCreationDTO();
        OrderCreationDTO secondOrderCreationDTO = generateOrderCreationDTO();

        //when
        orderService.placeOrder(firstOrderCreationDTO);
        orderService.placeOrder(secondOrderCreationDTO);
        List<OrderExistingDTO> existingOrders = orderService.findOrders(new Date(System.currentTimeMillis() - 100000l), null);

        //then
        Assert.assertEquals(2, existingOrders.size());
    }


    @Test
    public void persistAndFindOrdersWithNullDateFrom() {
        //given
        OrderCreationDTO firstOrderCreationDTO = generateOrderCreationDTO();
        OrderCreationDTO secondOrderCreationDTO = generateOrderCreationDTO();

        //when
        orderService.placeOrder(firstOrderCreationDTO);
        orderService.placeOrder(secondOrderCreationDTO);
        List<OrderExistingDTO> existingOrders = orderService.findOrders(null, new Date(System.currentTimeMillis() + 100000l));

        //then
        Assert.assertEquals(2, existingOrders.size());
    }

    OrderCreationDTO generateOrderCreationDTO() {
        List<ProductQuantityDTO> productQuantityDTOList = new ArrayList<>();
        productQuantityDTOList.add(new ProductQuantityDTO(existingFirstProduct.getId(), RandomUtils.generateRandomQuantity()));
        productQuantityDTOList.add(new ProductQuantityDTO(existingSecondProduct.getId(), RandomUtils.generateRandomQuantity()));
        return new OrderCreationDTO(RandomUtils.generateRandomReferenceCode(), RandomUtils.generateRandomEmail(), productQuantityDTOList);
    }

}
