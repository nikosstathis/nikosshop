package com.nikosstathis.nikosshop.model;

import static com.nikosstathis.nikosshop.util.RandomUtils.*;

import org.junit.Test;

import java.sql.Timestamp;

/**
 * PlacedOrderProductTestCase provides unit tests for the PlacedOrderProduct class.
 * The only thing implemented there is a toString() method
 */
public class PlacedOrderProductTestCase {

    @Test
    public void printPlacedOrderProduct() {
        System.out.println(new PlacedOrderProduct());
        System.out.print(new PlacedOrderProduct(
                1l,
                new Product(1l, generateRandomName(), generateRandomPrice()),
                new PlacedOrder(2l, generateRandomReferenceCode(), generateRandomEmail(), new Timestamp(0),
                        generateRandomPrice(), null),
                generateRandomQuantity(),
                generateRandomPrice()
        ));
    }

}
