package com.nikosstathis.nikosshop.model;

import static com.nikosstathis.nikosshop.util.RandomUtils.*;

import org.junit.Test;

import java.sql.Timestamp;

/**
 * PlacedOrderTestCase provides unit tests for the PlacedOrder class.
 * The only thing implemented there is a toString() method
 */
public class PlacedOrderTestCase {

    @Test
    public void printPlacedOrder() {
        System.out.println(new PlacedOrder());
        System.out.println(new PlacedOrder(1l, generateRandomReferenceCode(), generateRandomEmail(),
                new Timestamp(0), generateRandomPrice(), null));
    }

}
