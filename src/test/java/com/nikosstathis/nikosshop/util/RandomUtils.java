package com.nikosstathis.nikosshop.util;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

/**
 * RandomUtils provides some methods so as to add random data in some of our tests
 */
public abstract class RandomUtils {

    private static final double MIN_RANDOM_PRICE_LIMIT = 0;
    private static final double MAX_RANDOM_PRICE_LIMIT = 10000000;
    private static final long MIN_RANDOM_QUANTITY = 1;
    private static final long MAX_RANDOM_QUANTITY = 10;
    private static final String RANDOM_EMAIL_SUFFIX = "@nikosstathis.com";

    /**
     * Generates a random BigDecimal that can be used as a product price.
     * We limit the price between the MIN_RANDOM_PRICE_LIMIT and MAX_RANDOM_PRICE_LIMIT values
     *
     * @return the generated BigDecimal
     */
    public static BigDecimal generateRandomPrice() {
        return new BigDecimal(
                new Random()
                        .doubles(MIN_RANDOM_PRICE_LIMIT, MAX_RANDOM_PRICE_LIMIT)
                        .findFirst()
                        .getAsDouble()
        );
    }

    /**
     * Generates a random String that can be used as a product name.
     *
     * @return the generated String
     */
    public static String generateRandomName() {
        return UUID.randomUUID().toString();
    }

    /**
     * Generates a random long that can be used as a product quantity.
     * We limit the quantity between the MIN_RANDOM_QUANTITY and MAX_RANDOM_QUANTITY values
     *
     * @return the generated long
     */
    public static long generateRandomQuantity() {
        return new Random().longs(MIN_RANDOM_QUANTITY, MAX_RANDOM_QUANTITY).findFirst().getAsLong();
    }

    /**
     * Generates a random email, always ending with RANDOM_EMAIL_SUFFIX
     *
     * @return the generated email
     */
    public static String generateRandomEmail() {
        return UUID.randomUUID() + RANDOM_EMAIL_SUFFIX;
    }

    /**
     * Generates a random String that can be used as a reference code/
     *
     * @return the generated String
     */
    public static String generateRandomReferenceCode() {
        return UUID.randomUUID().toString();
    }

}
