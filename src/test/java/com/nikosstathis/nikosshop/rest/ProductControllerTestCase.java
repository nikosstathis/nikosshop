package com.nikosstathis.nikosshop.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikosstathis.nikosshop.dto.ProductCreateDTO;
import com.nikosstathis.nikosshop.dto.ProductDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * ProductControllerTestCase contains tests for all method of the @{@link ProductController}
 * The implementation is based on Mockito and the services are mocked, we do not load any Spring Context
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTestCase {

    private MockMvc mvc;

    @InjectMocks
    private ProductController productController;

    @Mock
    ProductService productService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(productController)
                .setControllerAdvice(new GlobalRestExceptionHandler())
                .build();
    }

    @Test
    public void findAllProducts() throws Exception {
        final List<ProductDTO> productDTOList = Stream.of(
                new ProductDTO(1l, "aProduct", new BigDecimal(1111.11).setScale(2, RoundingMode.HALF_UP)),
                new ProductDTO(2l, "anotherProduct", new BigDecimal(222.22).setScale(2, RoundingMode.HALF_UP)),
                new ProductDTO(2l, "aThirdProduct", new BigDecimal(333.33).setScale(2, RoundingMode.HALF_UP))
        ).collect(Collectors.toList());

        when(productService.findAll())
                .thenReturn(productDTOList);

        this.mvc.perform(get("/rest/product/findall"))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(productDTOList)));
    }

    @Test
    public void createProduct() throws Exception {
        when(productService.create(anyString(), any(BigDecimal.class)))
                .thenReturn(new ProductDTO(1, "aProduct", new BigDecimal("123.23")));

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductCreateDTO("aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                post("/rest/product/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("aProduct")))
                .andExpect(content().string(containsString("123.23")))
        ;
    }

    @Test
    public void updateProduct() throws Exception {
        when(productService.update(anyLong(), anyString(), any(BigDecimal.class)))
                .thenReturn(new ProductDTO(1, "aProduct", new BigDecimal("123.23")));

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductDTO(1l, "aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                put("/rest/product/update").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("aProduct")))
                .andExpect(content().string(containsString("123.23")))
        ;
    }

    @Test
    public void updateProductThatRaisesDataIntegrityException() throws Exception {
        when(productService.create(anyString(), any(BigDecimal.class)))
                .thenThrow(DataIntegrityViolationException.class);

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductCreateDTO("aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                post("/rest/product/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is(HttpStatus.CONFLICT.value()));    }

    @Test
    public void updateProductThatRaisesDataIntegrityExceptionWithParentCause() throws Exception {
        DataIntegrityViolationException dataIntegrityViolationException = new DataIntegrityViolationException("error", new DataIntegrityViolationException("this is the parent error"));
        when(productService.create(anyString(), any(BigDecimal.class)))
                .thenThrow(dataIntegrityViolationException);

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductCreateDTO("aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                post("/rest/product/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is(HttpStatus.CONFLICT.value()))
                .andExpect(content().string(containsString("this is the parent error")));
    }

    @Test
    public void updateProductThatRaisesDataIntegrityExceptionWithGrantParentCause() throws Exception {
        DataIntegrityViolationException dataIntegrityViolationException = new DataIntegrityViolationException("error",
                new DataIntegrityViolationException("this is the parent error", new DataIntegrityViolationException("this is the grantparent error")));
        when(productService.create(anyString(), any(BigDecimal.class)))
                .thenThrow(dataIntegrityViolationException);

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductCreateDTO("aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                post("/rest/product/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is(HttpStatus.CONFLICT.value()))
                .andExpect(content().string(containsString("this is the grantparent error")));
    }

    @Test
    public void updateProductThatRaisesIllegalArgumentException() throws Exception {
        when(productService.create(anyString(), any(BigDecimal.class)))
                .thenThrow(IllegalArgumentException.class);

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductCreateDTO("aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                post("/rest/product/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
    }

    @Test
    public void updateProductThatRaisesInvalidEntityIdException() throws Exception {
        when(productService.update(anyLong(), anyString(), any(BigDecimal.class)))
                .thenThrow(new InvalidEntityIdException("Invalid Entity"));

        final String requestBody = new ObjectMapper().writeValueAsString(new ProductDTO(1l, "aProduct", new BigDecimal("123.23")));

        this.mvc.perform(
                put("/rest/product/update").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
    }

}
