package com.nikosstathis.nikosshop.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikosstathis.nikosshop.dto.*;
import com.nikosstathis.nikosshop.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * OrderControllerTestCase contains tests for all method of the @{@link OrderController}
 * The implementation is based on Mockito and the services are mocked, we do not load any Spring Context
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderControllerTestCase {

    private MockMvc mvc;

    @InjectMocks
    private OrderController orderController;

    @Mock
    OrderService orderService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(orderController)
                .setControllerAdvice(new GlobalRestExceptionHandler())
                .build();
    }

    @Test
    public void findOrders() throws Exception {
        final List<OrderExistingDTO> existingOrders = Stream.of(
                new OrderExistingDTO(1l, "111222333", "nikos@nikosstathis.com", new BigDecimal(10000),
                        new Date(), Stream.of(
                        new OrderProductDTO(1l, "aProduct", 1l, new BigDecimal(100), new BigDecimal(100)),
                        new OrderProductDTO(2l, "anotherProduct", 2l, new BigDecimal(200), new BigDecimal(200)),
                        new OrderProductDTO(3l, "aThirdProduct", 3l, new BigDecimal(300), new BigDecimal(300))
                ).collect(Collectors.toList())),
                new OrderExistingDTO(2l, "333222333", "stathis@nikosstathis.com", new BigDecimal(20000),
                        new Date(), Stream.of(
                        new OrderProductDTO(2l, "anotherProduct", 2l, new BigDecimal(1200), new BigDecimal(1200)),
                        new OrderProductDTO(3l, "aThirdProduct", 3l, new BigDecimal(1300), new BigDecimal(1300))
                ).collect(Collectors.toList()))
        ).collect(Collectors.toList());

        when(orderService.findOrders(any(Date.class), any(Date.class)))
                .thenReturn(existingOrders);
        final String requestBody = new ObjectMapper().writeValueAsString(new OrderRequestDTO(new Date(), new Date()));

        this.mvc.perform(post("/rest/order/find").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(existingOrders)));
    }

    @Test
    public void placeOrder() throws Exception {
        final OrderExistingDTO createdOrder = new OrderExistingDTO(1l, "111222333", "nikos@nikosstathis.com", new BigDecimal(10000),
                new Date(), Stream.of(
                new OrderProductDTO(1l, "aProduct", 1l, new BigDecimal(100), new BigDecimal(100)),
                new OrderProductDTO(2l, "anotherProduct", 2l, new BigDecimal(200), new BigDecimal(200)),
                new OrderProductDTO(3l, "aThirdProduct", 3l, new BigDecimal(300), new BigDecimal(300))
        ).collect(Collectors.toList()));

        when(orderService.placeOrder(any(OrderCreationDTO.class)))
                .thenReturn(createdOrder);

        final String requestBody = new ObjectMapper().writeValueAsString(new OrderCreationDTO("111222333", "nikos@nikosstathis.com",
                Stream.of(new ProductQuantityDTO(1l, 1l), new ProductQuantityDTO(2l, 1l)).collect(Collectors.toList())));
        this.mvc.perform(post("/rest/order/create").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(createdOrder)));
    }

    @Test
    public void recalculateOrderPrice() throws Exception {
        RecalculateOrderPriceResultDTO result = new RecalculateOrderPriceResultDTO(1l, new BigDecimal(100), new BigDecimal(200));

        when(orderService.recalculateOrderPriceBasedOnCurrentProductPrices(anyLong()))
                .thenReturn(result);

        final String requestBody = new ObjectMapper().writeValueAsString(new RecalculateOrderPriceRequestDTO(1l));
        this.mvc.perform(put("/rest/order/recalculatePriceBasedOnCurrentProductPrices").contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(result)));
    }

}
