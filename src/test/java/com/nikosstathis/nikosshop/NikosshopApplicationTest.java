package com.nikosstathis.nikosshop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * NikosshopApplicationTest test that the spring context can be loaded
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NikosshopApplicationTest {

    @Test
    public void contextLoads() {
    }

}
