package com.nikosstathis.nikosshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * This is the class defining the entry point of the application
 */
@SpringBootApplication(scanBasePackages = {"com.nikosstathis.nikosshop"})
@EnableJpaRepositories(basePackages = "com.nikosstathis.nikosshop.repository")
@EntityScan("com.nikosstathis.nikosshop.model")
public class NikosshopApplication {
    /**
     * This is the entry point of our app
     *
     * @param args (no arguments are needed for our app)
     */
    public static void main(String[] args) {
        SpringApplication.run(NikosshopApplication.class, args);
    }
}
