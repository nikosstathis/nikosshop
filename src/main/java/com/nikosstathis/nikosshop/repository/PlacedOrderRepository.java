package com.nikosstathis.nikosshop.repository;

import com.nikosstathis.nikosshop.model.PlacedOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * The repository for the PlacedOrder entities.
 * Some new methods were needed except for the default JpaRepository ones,
 * so as to be able to find results based on dates. Spring Data Jpa handles
 * the implementation.
 */
@Repository
public interface PlacedOrderRepository extends JpaRepository<PlacedOrder, Long> {

    /**
     * findByCreationDateBetween method is used in order to return orders that were created during a time interval
     *
     * @param from used to indicate the starting date
     * @param to   used to indicate the ending date
     * @return a list of Placed orders created inside the time interval
     */
    List<PlacedOrder> findByCreationDateBetween(Date from, Date to);


    /**
     * findByCreationDateBefore method is used in order to return orders that were created before a specific time
     *
     * @param to used to indicate the ending date
     * @return a list of Placed orders created before the time indicated by the 'to' param
     */
    List<PlacedOrder> findByCreationDateBefore(Date to);

    /**
     * findByCreationDateBetween method is used in order to return orders that were created during a time interval
     *
     * @param from used to indicate the starting date
     * @return a list of Placed orders created inside the time interval
     */
    List<PlacedOrder> findByCreationDateAfter(Date from);

}
