package com.nikosstathis.nikosshop.repository;

import com.nikosstathis.nikosshop.model.PlacedOrderProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the PlacedOrderProduct entities.
 * New methods were not needed, using the JpaRepository defaults
 */
@Repository
public interface PlacedOrderProductRepository extends JpaRepository<PlacedOrderProduct, Long> {

}
