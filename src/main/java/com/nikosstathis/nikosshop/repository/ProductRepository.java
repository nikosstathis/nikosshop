package com.nikosstathis.nikosshop.repository;

import com.nikosstathis.nikosshop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the Product entities.
 * New methods were not needed, using the JpaRepository defaults
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
