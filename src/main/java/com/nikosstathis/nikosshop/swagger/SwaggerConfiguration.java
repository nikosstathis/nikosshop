package com.nikosstathis.nikosshop.swagger;

import org.springframework.context.annotation.*;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ComponentScan({"com.nikosstathis.nikosshop.rest.ProductController"})
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {
    private static final String SWAGGER_API_VERSION = "1.0";
    private static final String TITLE = "NikosShop RESTful API";
    private static final String DESCRIPTION = "The RESTful API of the NikosShop application";

    @Bean
    public Docket productsApi(){
        Docket ret =  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title(TITLE)
                        .description(DESCRIPTION)
                        .version(SWAGGER_API_VERSION)
                        .build()
                )
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.nikosstathis.nikosshop.rest"))
                .paths(PathSelectors.regex("/rest.*"))
                .build();
        ret.useDefaultResponseMessages(false);
        return ret;
    }

}
