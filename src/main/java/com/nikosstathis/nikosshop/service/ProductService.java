package com.nikosstathis.nikosshop.service;

import com.nikosstathis.nikosshop.dto.ProductDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * ProductService is the interface that defines all the methods needed for our service
 * that handles (creates, updates, searches) all the products
 */
@Service
public interface ProductService {

    /**
     * findAll returns all the products
     *
     * @return a list with the existing products
     */
    List<ProductDTO> findAll();

    /**
     * creates a new product
     *
     * @param name  the product name, not nullable, should be unique per product else a {@link org.springframework.dao.DataIntegrityViolationException }
     *              will be thrown
     * @param price the price name, not nullable
     * @return the DTO of the created product, containing the created DB id of the product
     */
    ProductDTO create(String name, BigDecimal price);

    /**
     * updates an existing product
     *
     * @param productId the actual id of the product
     * @param name      the new name we want to give to the product (null is accepted defining that we do not want to modify
     *                  the existing name)
     * @param price     the new price we want to give to the product (null is accepted defining  that  we do not want to
     *                  modify the existing price)
     * @return the updated prooduct
     * @throws InvalidEntityIdException when no product exists with this id
     */
    ProductDTO update(long productId, String name, BigDecimal price) throws InvalidEntityIdException;

}