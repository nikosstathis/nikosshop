package com.nikosstathis.nikosshop.service;

import com.nikosstathis.nikosshop.dto.ProductDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.model.Product;
import com.nikosstathis.nikosshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ProductServiceJpa is the JPA implementation of the ProductService based on Spring Data JPA
 */
@Service
public class ProductServiceJpa implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductDTO> findAll() {
        return productRepository.findAll().stream().map(ProductDTO::new).collect(Collectors.toList());
    }

    @Override
    public ProductDTO create(String name, BigDecimal price) {
        return new ProductDTO(
                productRepository.save(new Product(null, name, price.setScale(2, RoundingMode.HALF_UP)))
        );
    }

    @Override
    public ProductDTO update(long productId, String name, BigDecimal price) throws InvalidEntityIdException {
        Product existingProduct = productRepository
                .findById(productId)
                .orElseThrow(() -> new InvalidEntityIdException("no product with id:" + productId));
        if (name != null) {
            existingProduct.setName(name);
        }
        if (price != null) {
            existingProduct.setPrice(price.setScale(2, RoundingMode.HALF_UP));
        }
        return new ProductDTO(
                productRepository.save(existingProduct)
        );
    }

}
