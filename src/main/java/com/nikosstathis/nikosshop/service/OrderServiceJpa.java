package com.nikosstathis.nikosshop.service;

import com.nikosstathis.nikosshop.dto.*;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.model.PlacedOrder;
import com.nikosstathis.nikosshop.model.PlacedOrderProduct;
import com.nikosstathis.nikosshop.model.Product;
import com.nikosstathis.nikosshop.repository.PlacedOrderRepository;
import com.nikosstathis.nikosshop.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OrderServiceJpa is the JPA implementation of the OrderService based on Spring Data JPA
 */
@Service
public class OrderServiceJpa implements OrderService{

    @Autowired
    private PlacedOrderRepository placedOrderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<OrderExistingDTO> findOrders(Date from , Date to) {
        List<PlacedOrder> placedOrderList;
        if(from!=null){
            if(to!=null){ //both dates not empty
                placedOrderList = placedOrderRepository.findByCreationDateBetween(from, to);
            } else { // 'to' date is empty
                placedOrderList = placedOrderRepository.findByCreationDateAfter(from);
            }
        } else {
            if (to!=null){ // 'from' date is empty
                placedOrderList = placedOrderRepository.findByCreationDateBefore(to);
            } else{ //both dates empty
                placedOrderList = placedOrderRepository.findAll();
            }
        }

        return placedOrderList.stream()
                .map(OrderServiceJpa::entityOrderToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public OrderExistingDTO placeOrder(OrderCreationDTO orderCreationDTO) {

        PlacedOrder placedOrder = new PlacedOrder();
        placedOrder.setBuyerEmail(orderCreationDTO.getBuyerEmail());
        placedOrder.setReferenceCode(orderCreationDTO.getReferenceCode());
        placedOrder.setCreationDate(new Timestamp(System.currentTimeMillis()));

        //get all products
        List<Long> productIds = orderCreationDTO.getProducts()
                .stream()
                .map(ProductQuantityDTO::getProductId)
                .collect(Collectors.toList());

        List<Product> existingProducts = productRepository.findAllById(productIds);

        //ok, we have valid productIds, lets create the placed_order entry and the placed_order_product entries

        orderCreationDTO.getProducts()
                .stream()
                .map(productWithQuantityDTO -> createPlacedOrderProduct(productWithQuantityDTO, existingProducts))
                .forEach(placedOrder::addPlacedOrderProduct);
        return entityOrderToDTO(
                placedOrderRepository.save(placedOrder)
        );

    }

    @Override
    public RecalculateOrderPriceResultDTO recalculateOrderPriceBasedOnCurrentProductPrices(long placedOrderId) throws InvalidEntityIdException {
        PlacedOrder order = placedOrderRepository.findById(placedOrderId)
                .orElseThrow(() -> new InvalidEntityIdException("no placed order with id:"+placedOrderId));

        //we need to update:
        // - the final price of the placedOrder
        // - the price of  the placedOrderProduct entries

        BigDecimal oldTotalPrice = order.getTotalPrice();
        //reset the initial value
        order.setTotalPrice(new BigDecimal(0));
        order.getPlacedOrderProductList()
                .stream()
                .forEach(
                    placedOrderProduct -> {
                        placedOrderProduct.setPrice(placedOrderProduct.getProduct().getPrice());
                        order.setTotalPrice(order.getTotalPrice().add(placedOrderProduct.getProduct().getPrice().multiply(new BigDecimal(placedOrderProduct.getQuantity()))));
                    }
                );

        //save the updated values
        placedOrderRepository.save(order);

        return new RecalculateOrderPriceResultDTO(placedOrderId, oldTotalPrice, order.getTotalPrice());
    }

    PlacedOrderProduct createPlacedOrderProduct(ProductQuantityDTO productWithQuantityDTO, List<Product> existingProductList) {
        PlacedOrderProduct placedOrderProduct = new PlacedOrderProduct();
        placedOrderProduct.setQuantity(productWithQuantityDTO.getQuantity());
        Product product = existingProductList.stream()
                .filter(existingProduct -> existingProduct.getId() == productWithQuantityDTO.getProductId())
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid ProductId:" + productWithQuantityDTO.getProductId() + " in order"));
        placedOrderProduct.setProduct(product);
        placedOrderProduct.setPrice(product.getPrice());//we set the price here because the product's price may change

        return placedOrderProduct;
    }

    private static OrderExistingDTO entityOrderToDTO(PlacedOrder order){

        return new OrderExistingDTO(
                order.getId(),
                order.getReferenceCode(),
                order.getBuyerEmail(),
                order.getTotalPrice().setScale(2, RoundingMode.HALF_UP),
                new Date(order.getCreationDate().getTime()),
                order.getPlacedOrderProductList()
                        .stream()
                        .map(OrderServiceJpa::entityProductToDTO)
                        .collect(Collectors.toList()));

    }

    private static OrderProductDTO entityProductToDTO(PlacedOrderProduct placedOrderProduct){
        return new OrderProductDTO(
                placedOrderProduct.getProduct().getId(),
                placedOrderProduct.getProduct().getName(),
                placedOrderProduct.getQuantity(),
                placedOrderProduct.getPrice().setScale(2, RoundingMode.HALF_UP),
                placedOrderProduct.getProduct().getPrice()
        );
    }

}
