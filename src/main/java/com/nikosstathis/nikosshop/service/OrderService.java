package com.nikosstathis.nikosshop.service;

import com.nikosstathis.nikosshop.dto.OrderCreationDTO;
import com.nikosstathis.nikosshop.dto.OrderExistingDTO;
import com.nikosstathis.nikosshop.dto.RecalculateOrderPriceResultDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * OrderService is the interface that defines all the methods needed for our service
 * that handles (creates, updates, searches) all the orders
 */
@Service
public interface OrderService {

    /**
     * findOrders returns all the orders that were created within a time period.
     *
     * @param from defines that start of the period, null is acceptable, indicates that we do not need
     *             to take into consideration any starting date
     * @param to   defines the end of the period, null is acceptable, indicates that we do not need
     *             to take into consideration any ending date
     * @return a list of all the orders created within the requested period
     */
    List<OrderExistingDTO> findOrders(Date from, Date to);


    /**
     * placeOrder creates the actual order, stores it in the database and returns the stored order info
     *
     * @param orderCreationDTO the DTO for the order creation that contains the buyer's email, a reference
     *                         code unique for each order and the products listed in this order that should
     *                         be unique for each order (eitherwise a {@link org.springframework.dao.DataIntegrityViolationException}
     *                         will be thrown)
     * @return all the info after the order creation, containing the DB id of the order and the creation date of the order
     */
    OrderExistingDTO placeOrder(OrderCreationDTO orderCreationDTO);

    /**
     * the product prices may be modified after an order has been placed. With the recalculateOrderPriceBasedOnCurrentProductPrices
     * we recalculate the total order price based on the current product prices and set the new price to the order
     *
     * @param placedOrderId the DB id of the existing order
     * @return a dto containing the orderId, the new order price (which has been set now as the current price)and the old price
     *
     * @throws InvalidEntityIdException
     */
    RecalculateOrderPriceResultDTO recalculateOrderPriceBasedOnCurrentProductPrices(long placedOrderId) throws InvalidEntityIdException;

}