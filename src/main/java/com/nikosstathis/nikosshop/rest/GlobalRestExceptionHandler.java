package com.nikosstathis.nikosshop.rest;

import com.nikosstathis.nikosshop.dto.ControllerExceptionError;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;

/**
 * GlobalRestExceptionHandler is used so as to handle all rest exceptions in a uniform  way, responding with a
 * @{@link ControllerExceptionError}
 *
 */
@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex.getMessage(), ((ServletWebRequest) request).getRequest().getServletPath());
    }

    @ExceptionHandler({InvalidEntityIdException.class})
    public ResponseEntity<Object> handleInvalidEntityIdException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, ex.getMessage(), ((ServletWebRequest) request).getRequest().getServletPath());
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(Exception ex, WebRequest request) {
        String message = ex.getMessage();
        if (ex.getCause() != null) {//dive into the messages :)
            if (ex.getCause().getCause() != null) {
                message = ex.getCause().getCause().getMessage();
            } else {
                message = ex.getCause().getMessage();
            }
        }
        return createResponseEntity(HttpStatus.CONFLICT, message, ((ServletWebRequest) request).getRequest().getServletPath());
    }

    ResponseEntity<Object> createResponseEntity(HttpStatus httpStatus, String message, String requestPath) {
        return new ResponseEntity<>(
                new ControllerExceptionError(
                        new Timestamp(System.currentTimeMillis()) + "",
                        httpStatus.value(),
                        httpStatus.name(),
                        message,
                        requestPath
                ), new HttpHeaders(), httpStatus
        );
    }

}
