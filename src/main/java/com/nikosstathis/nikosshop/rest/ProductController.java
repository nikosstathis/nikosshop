package com.nikosstathis.nikosshop.rest;

import com.nikosstathis.nikosshop.dto.ControllerExceptionError;
import com.nikosstathis.nikosshop.dto.ProductCreateDTO;
import com.nikosstathis.nikosshop.dto.ProductDTO;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * ProductController is the RESTful entry point for all the product services
 */
@RestController
@RequestMapping("/rest/product/")
@Api(value = "ProductController API", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * RESTful service used to request all the existing products
     *
     * @return a list of the existing products
     */
    @GetMapping("findall")
    @ApiOperation(value = "Returns all the existing products", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({@ApiResponse(code = 200, message = "OK")})
    public List<ProductDTO> findAllProducts() {
        return productService.findAll();
    }

    /**
     * RESTful service used to create a new product
     *
     * @param product is a @{@link ProductCreateDTO } containing the name and price for the new product
     * @return a @{@link ProductDTO} containing the created product
     */
    @PostMapping("create")
    @ApiOperation(value = "Create new product", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 409, message = "Conflict", response = ControllerExceptionError.class)
    })
    public ProductDTO createProduct(@Valid @RequestBody ProductCreateDTO product) {
        return productService.create(product.getName(), product.getPrice());
    }

    /**
     * RESTful service used to update an existing product
     *
     * @param product is a @{@link ProductDTO} containing the id of the existing product, the new name and the new price
     *                we want the existing product to have. Then name and/or price can have a null value indicating
     *                that we do not want this parameter to be modified
     * @return a @{@link ProductDTO} with the updated product
     * @throws InvalidEntityIdException when the requested productId does not correspond to the id of a valid product
     */
    @PutMapping("update")
    @ApiOperation(value = "Update existing product", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 409, message = "Conflict", response = ControllerExceptionError.class),
            @ApiResponse(code = 422, message = "Unprocessable Entity", response = ControllerExceptionError.class)
    })
    public ProductDTO updateProduct(@Valid @RequestBody ProductDTO product) throws InvalidEntityIdException {
        return productService.update(product.getId(), product.getName(), product.getPrice());
    }

}
