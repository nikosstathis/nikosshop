package com.nikosstathis.nikosshop.rest;

import com.nikosstathis.nikosshop.dto.*;
import com.nikosstathis.nikosshop.exception.InvalidEntityIdException;
import com.nikosstathis.nikosshop.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * OrderController is the RESTful entry point for all the order services
 */
@RestController
@RequestMapping("/rest/order/")
@Api(value = "OrderController API", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * RESTful service used to request the existing orders for a specific period
     *
     * @param orderRequestDTO contains the from and to dates that define the period. Nullable dates are valid when we
     *                        do not want to specify one or both of the dates
     * @return a list of @{@link OrderExistingDTO } with all the existing orders for this period
     */
    @PostMapping("find")
    @ApiOperation(value = "finds all the existing orders for a specific period", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request")
    })
    public List<OrderExistingDTO> findOrders(@RequestBody OrderRequestDTO orderRequestDTO) {
        return orderService.findOrders(orderRequestDTO.getCreationDateUtcFrom(), orderRequestDTO.getCreationDateUtcTo());
    }

    /**
     * RESTful service used to create a new order
     *
     * @param orderCreationDTO contains a unique referenceCode for the  new order, the buyer's email, and the product listt
     * @return a @{@link OrderExistingDTO} containing all the data of the created order
     */
    @PostMapping("create")
    @ApiOperation(value = "creates a new order", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 409, message = "Conflict", response = ControllerExceptionError.class),
            @ApiResponse(code = 422, message = "Unprocessable Entity", response = ControllerExceptionError.class)
    })
    public OrderExistingDTO placeOrder(@RequestBody OrderCreationDTO orderCreationDTO) {
        return orderService.placeOrder(orderCreationDTO);
    }

    /**
     * RESTful service used to update the total price of an order according to the current product prices
     *
     * @param recalculateOrderPriceDTO contains the id of the order that we want to recalculate
     * @return a @{@link RecalculateOrderPriceResultDTO} containing  the orderId, the old price and the new price
     * @throws InvalidEntityIdException when the orderId in the request does not respond to a valid order
     */
    @PutMapping("recalculatePriceBasedOnCurrentProductPrices")
    @ApiOperation(value = "updates the total price of an order according to the current product prices", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity", response = ControllerExceptionError.class)
    })
    public RecalculateOrderPriceResultDTO setRecalculateOrderPrice(@Valid @RequestBody RecalculateOrderPriceRequestDTO recalculateOrderPriceDTO) throws InvalidEntityIdException {
        return orderService.recalculateOrderPriceBasedOnCurrentProductPrices(recalculateOrderPriceDTO.getPlacedOrderId());
    }

}
