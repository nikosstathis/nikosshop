package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * ProductCreateDTO is used for product creation requests.
 *
 * name is used to indicate the product name and should be unique per product. Cannot exceed 255 characters
 * price is used to indicate the product price. In the DB we will store up to 4 decimal digits. Zero is
 * accepted because we could have some free products
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ProductCreateDTO is used used for product creation requests")
public class ProductCreateDTO {

    @NotNull
    @Size(max = 255)
    @ApiModelProperty(notes = "The product name, given by the user, should be unique per product", example = "aProduct")
    String name;

    @NotNull
    @PositiveOrZero
    @ApiModelProperty(notes = "The product price, given by the user", example = "1000.55")
    BigDecimal price;

}
