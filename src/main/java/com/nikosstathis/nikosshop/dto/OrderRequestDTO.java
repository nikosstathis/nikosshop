package com.nikosstathis.nikosshop.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * OrderRequestDTO is used to define the time period for the request that returns the existing orders.
 * The dates should be in UTC and in the yyyy-MM-dd HH:mm:ss pattern
 *
 * creationDateUtcFrom is the starting date of the period (null is accepted when we do not want to define
 * a starting date)
 * creationDateUtcT is the ending date of the period (null is accepted when we do not want to define
 * an ending date)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "used to define the time period for the request that returns the existing orders")
public class OrderRequestDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT")
    @ApiModelProperty(notes = "is the starting date of the period in pattern: \"yyyy-MM-dd HH:mm:ss\"(null is accepted when we do not want to define" +
            " a starting date)", example = "2018-01-01 10:12:25")
    Date creationDateUtcFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT")
    @ApiModelProperty(notes = "is the ending date of the period in pattern: \"yyyy-MM-dd HH:mm:ss\"(null is accepted when we do not want to define" +
            " an ending date)", example = "2019-01-01 17:10:20")
    Date creationDateUtcTo;
}
