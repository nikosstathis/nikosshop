package com.nikosstathis.nikosshop.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * OrderExistingDTO is the return DTO whenever a request for orders is made
 *
 * orderId is the DB id of the order
 * referenceCode is the reference code that was provided by the user on the order creation request
 * buyerEmail is the email of the user what was provided by the user on the order creation request
 * totalPrice was calculated by the application based on the product prices at the time of the order creation
 * creationDateUtc is the UTC date that the order was created in this format: yyyy-MM-dd HH:mm:ss
 * products is the list of @{@link OrderProductDTO} of this order
 */
@Data
@AllArgsConstructor
@ApiModel(description = "the return DTO whenever a request for orders is made")
public class OrderExistingDTO {

    @ApiModelProperty(notes = "The id of the order", example = "1")
    Long orderId;

    @ApiModelProperty(notes = "The reference code of the order", example = "REF123444")
    String referenceCode;

    @ApiModelProperty(notes = "The email of the buyer", example = "nikosstathis@gmail.com")
    String buyerEmail;

    @ApiModelProperty(notes = "The total price of the order", example = "1000.45")
    BigDecimal totalPrice;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT")
    @ApiModelProperty(notes = "The creation date of the order in UTC", example = "2018-01-10 23:00:23")
    Date creationDateUtc;

    @ApiModelProperty(notes = "products that exist in the order")
    List<OrderProductDTO> products;

}
