package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * RecalculateOrderPriceResultDTO is the result of the order price recalculation
 *
 * placedOrderId is the order.id
 * oldPrice is the old price that no longer exists
 * newPrice is the price after the recalculation based on current product prices
 */
@Data
@AllArgsConstructor
@ApiModel(description = "the result of the order price recalculation")
public class RecalculateOrderPriceResultDTO {

    @ApiModelProperty(notes = "Unique identifier of the order", example = "1")
    Long placedOrderId;

    @ApiModelProperty(notes = "the price the order had before the recalculation", example = "100.23")
    BigDecimal oldPrice;

    @ApiModelProperty(notes = "the price the order has now, after the recalculation", example = "150.23")
    BigDecimal newPrice;

}
