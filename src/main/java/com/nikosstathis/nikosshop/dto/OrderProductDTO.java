package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

/**
 * OrderProductDTO contains information regarding the product that exists in an order
 *
 * id is the product id
 * name is the product name
 * quantity shows how many times this product exists in the order
 * priceOnOrder is the price the product had when the order was placed
 * priceCurrent is the price the product has now
 */
@Data
@AllArgsConstructor
@ApiModel(description = "contains information regarding the product that exists in an order")
public class OrderProductDTO {

    @ApiModelProperty(notes = "the id of the product", example = "1")
    Long id;

    @ApiModelProperty(notes = "the name of the product", example = "aProduct")
    String name;

    @ApiModelProperty(notes = "how many times this product exists in the order", example = "10")
    Long quantity;

    @ApiModelProperty(notes = "the price the product had when the order was placed", example = "100.23")
    BigDecimal priceOnOrder;

    @ApiModelProperty(notes = "the price the product has now", example = "90.23")
    BigDecimal priceCurrent;

}
