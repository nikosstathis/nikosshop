package com.nikosstathis.nikosshop.dto;

import com.nikosstathis.nikosshop.model.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * ProductDTO is used as a result for existing product requests and also in order to update existing products
 * It contains the actual values of the DB @{@link Product } entity with some rounding  on the  price
 *
 * id is the DB product.id of the product
 * name is the product name given by the user on the product creation request
 * price is the product price given by the user on the product creation request (can be zero because there could be
 * some free products)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ProductDTO is used as a result for existing product requests " +
        "and also in order to update existing products")
public class ProductDTO {

    @Positive
    @ApiModelProperty(notes = "Unique identifier of the product", example = "1")
    long id;

    @Size(min = 2, max = 255)
    @ApiModelProperty(notes = "The product name, given by the user, should be unique per product", example = "aProduct")
    String name;

    @PositiveOrZero
    @ApiModelProperty(notes = "The product price, given by the user", example = "1000.55")
    BigDecimal price;

    public ProductDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice().setScale(2, RoundingMode.HALF_UP);
    }

}
