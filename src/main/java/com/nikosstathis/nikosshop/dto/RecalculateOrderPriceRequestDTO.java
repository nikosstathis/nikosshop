package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;

/**
 * RecalculateOrderPriceRequestDTO is used when we want to send the request for recalculation of
 * the order price
 *
 * placedOrderId is the id of the existing order, should correspond to a valid id
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "used when we want to send the request for recalculation of" +
        " the order price based on the current order prices")
public class RecalculateOrderPriceRequestDTO {
    @Positive
    @ApiModelProperty(notes = "Unique identifier of the product", example = "1")
    long placedOrderId;
}
