package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;

/**
 * ProductQuantityDTO is used on order creation to define an existing product id and how many times
 * it will be contained in the order
 *
 * productId is the DB product.id
 * quantity is a long defining how many times the same product is bought
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "used on order creation to define an existing product id and how many times" +
        " it will be contained in the order")
public class ProductQuantityDTO {

    @Positive
    @ApiModelProperty(notes = "the id of the product", example = "1")
    long productId;

    @Positive
    @ApiModelProperty(notes = "indicates how many time the product exist in the order", example = "10")
    long quantity;

}
