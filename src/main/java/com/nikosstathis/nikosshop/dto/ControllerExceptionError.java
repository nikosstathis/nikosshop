package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ControllerExceptionError is used so as to have uniform detailed message on errors we
 * get calling the Restful services providing info on what caused the error.
 * The variable names were taken from the default Spring MVC error messages so that
 * everything is uniform.
 *
 * timestamp returns the time of the incident
 * status is the id of the HTTP status error code
 * error is the description of the HTTP status error code
 * message is a detailed message that may help the used understand what went wrong
 * path is the request path
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ControllerExceptionError is used so as to have uniform detailed message on errors we" +
        " * get calling the Restful services providing info on what caused the error." +
        " * The variable names were taken from the default Spring MVC error messages so that" +
        " * everything is uniform.")
public class ControllerExceptionError {

    @ApiModelProperty(notes = "the time of the incident", example = "2018-12-08 02:41:21.674")
    String timestamp;

    @ApiModelProperty(notes = "the id of the HTTP status error code", example = "409")
    int status;

    @ApiModelProperty(notes = "the description of the HTTP status error code", example = "CONFLICT")
    String error;

    @ApiModelProperty(notes = "a detailed message that may help the used understand what went wrong",
            example = "(conn=1996) Duplicate entry 'another product0' for key 'name'")
    String message;

    @ApiModelProperty(notes = "the request path", example = "/rest/product/create")
    String path;

}
