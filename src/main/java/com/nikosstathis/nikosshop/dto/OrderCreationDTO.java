package com.nikosstathis.nikosshop.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * OrderCreationDTO contains the info for the order creation request.
 *
 * referenceCode should be unique per order, not longer than 255 chars
 * buyerEmail should have the email format, not long than 255 chars
 * products oontains a list of @{@link ProductQuantityDTO} should not be null
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreationDTO {

    @NonNull
    @Size(max = 255)
    @ApiModelProperty(notes = "a reference code provided by the user that should be unique per order", example = "REF1230001")
    String referenceCode;

    @NotNull
    @Email
    @Size(max = 255)
    @ApiModelProperty(notes = "the email of the buyer", example = "nikosstathis@gmail.com")
    String buyerEmail;

    @NonNull
    @ApiModelProperty(notes = "a list of products-quantities")
    List<ProductQuantityDTO> products;

}
