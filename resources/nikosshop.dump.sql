#1. create the DB
CREATE DATABASE IF NOT EXISTS `nikosshop` ;
USE `nikosshop`;

#2. create the tables
CREATE TABLE IF NOT EXISTS `placed_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'the primary key of the table',
  `reference_code` varchar(255) NOT NULL COMMENT 'this is the reference code that can be used to identify the order (e.g. by the user) so that the user will not have access of the table''s id. Will be  filled by values from method UUID.randomUUID() ',
  `buyer_email` varchar(255) NOT NULL COMMENT 'the email of the buyer',
  `creation_date` datetime NOT NULL COMMENT 'the date the order was placed',
  `total_price` decimal(13,4) DEFAULT NULL COMMENT 'the total price, sum of all products',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reference_code` (`reference_code`),
  KEY `buyer_email` (`buyer_email`),
  KEY `creation_date` (`creation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table stores the orders placed in the system';

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'the primary key of the talbe',
  `name` varchar(255) DEFAULT NULL COMMENT 'the actual name of the product, careful so that it does not  exceed 255 chars',
  `price` decimal(13,4) DEFAULT NULL COMMENT 'the current price of the product, this may change over time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table stores the actual products and their current quotes';

CREATE TABLE IF NOT EXISTS `placed_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'the primary key of the table',
  `placed_order_id` int(11) NOT NULL COMMENT 'the foreign key to the placed_order table',
  `product_id` int(11) NOT NULL COMMENT 'the foreign key to the  product table',
  `quantity` int(11) NOT NULL COMMENT 'shows how many times the product existis in the order',
  `price` decimal(13,4) NOT NULL COMMENT 'the price of the product at the time the order was placed ( this will not change if the product.price changes)',
  PRIMARY KEY (`id`),
  KEY `placed_order_id` (`placed_order_id`),
  KEY `FK_PRODUCT_ID` (`product_id`),
  CONSTRAINT `FK_PLACED_ORDER_ID` FOREIGN KEY (`placed_order_id`) REFERENCES `placed_order` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_PRODUCT_ID` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table stores the many-to-many relationships  between the products and the orders';

#3. create the DB account for the application
CREATE USER 'nikosapp' IDENTIFIED BY 'nikos123';
GRANT SELECT,INSERT,UPDATE ON nikosshop.* TO 'nikosapp'@'%' identified  by 'nikos123'
