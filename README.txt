**1. Introduction**

This is the backend of an eshop implementing products and  orders. 

**2. Architecture**

- This is a Spring Boot application. 
- It runs on Java 8 or newer versions of Java
- It is a maven project
- I have selected MariaDB as the solution for storing the data because in the job description it was mentioned that this is the database you use in your stack
- Spring Data JPA was used so as to create the CRUD operations for the data
- lombok is used to generate methods for all the POJOs
- H2 DB was used as an embedded database for the integration tests of the services
- Mockito and MockMvc was used for the controller unit tests. 
- Swagger was used so as to create the documentation and be able to test the RESTful services 
- Spring Boot Actuator has been integrated so that the application has some endpoints that help us monitor data, statistics and also the health of the application
- slf4j is used for logging with logback as the implementation

**3. Installation**

The steps in order to install the application are:
1. Create the DB data
- create the database
- create the tables 
- create the account that the application is going to use and give select/insert/update access to the new account for the new database

The file nikosshop.dump.sql in the folder resources has all the needed queries for the above actions. It should be run by a DB admin account, or an account that has access to create databases, tables, users and give access to users. 

2. Build the application 

go to the application directory  and run: 

`mvn clean install`


3. Modify the application properties so that it can access the MariaDB database. 

If the MariaDB service is running in the same host as the application, this step should be discarded.
Else modify the property spring:datasource:url which now is :  
> spring:
>   datasource:
>     url: jdbc:mariadb://localhost:3306/nikosshop


**4. Running the application** 

The application will create a web server listening on port 8081, if this is not suitable for you (e.g. another service is already running on this port) you can modify this by opening the the application.yml file and modifying the server port property:

> server:
>   port: 8081

In order to run the application go to the directory of the application and run:

`java -jar target/nikosshop-0.0.1.jar`

or using maven:

`mvn exec:exec`

If everything has gone as expected you should be a message like this in the end of the console logs: 

> 09-12-2018 06:23:25.465 [main] INFO  c.n.nikosshop.NikosshopApplication.logStarted - Started NikosshopApplication in 8.147 seconds (JVM running for 8.899) 

To validate that the app is up running, you can check the actuator info page by opening a web brower on this page:

`http://localhost:8081/actuator/info`

where the server should respond this this DTO (with a different timestamp of course ) : 
>  {"build":{"artifact":"nikosshop","name":"nikosshop","time":"2018-12-09T04:02:19.262Z","version":"0.0.1","group":"com.nikosstathis"}}

**5. Testing the application**

There are 2 ways to easily test the application
- using the Swagger UI
open the embedded swagger-ui page in your browser: 

http://localhost:8081/swagger-ui.html


- using Postman 

you can import the file nikosshop.postman_collection.json from the resources directory in Postman and then you should be ready to test

**6. Test Cases**

One series of actions that should cover most functionality would be the following: 

- create a product with name product1
- create a product with name product2
- call the service to get all products
- update the second product's name and price
- try to update the second product's name to product1. This should give you an http error with status 409 (Conflict)
- try to update a product with a productId that does not exist. This should give you an http error with  status 422 (Unprocessable Entity)

- create a new order containing the 2 products
- create another order containing one of the 2 products
- try to create a third order by giving the same referenceCode you have given for one of the 2 already created orders. This should give you an http error with status 409 (Conflict)
- call the service to get all the orders for various periods of time
- modify the price of product1
- call the service to get all the orders, check that the order's price has not been modified even though the product's price has been modified
- call the method to recalculate the order price based on the current product prices
- call the service to get all the orders, check that the order's price has now been modified

**Appendix: The DB model**
- The application data model consists of 1 database (nikosshop) that contains 3  tables (product, placed_order, placed_order_product).
- All 3 tables have auto-generated ids
- The product table has 3 columns, the id, the name and the price. There is a unique constraint on the name so that 2 products cannot have the same name 
- The placed_order table has 3 columns, the id, the reference_code, the buyer_email, the creation date and the total_price. The reference code has a unique constraint  so that it can be provided by the user as a unique identifier for this order
- The placed_order_product table has 5 columns, the id, the placed_order_id which is a foreign key to the placed_order.id, the product_id which is a  foreign key to the product.id, the quantity which shows how many times this product exists in this order and the price which is the price the product had on the order creation


